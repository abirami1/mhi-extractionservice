﻿using System;
using System.ServiceProcess;
using System.Timers;

namespace mhiextractionService
{
    public partial class ExtractionService : ServiceBase
    {
        public ExtractionService()
        {
            InitializeComponent();
        }
        static DBOfficeFileSearch _dbContentSearch;

        private static void timerStart()
        {
            Global.setTimer();
            Global.timer.Interval = (60000 * 5);
            Global.timer.AutoReset = false;
            Global.timer.Elapsed += DispatcherTimer_Tick;

            Global.timer.Start();
            DispatcherTimer_Tick(null, null);

            //timer = new Timer(DispatcherTimer_Tick, null, 1000, Timeout.Infinite);
        }


        private static void DispatcherTimer_Tick(Object source, ElapsedEventArgs e)
        {
            try
            {
                EventViewer.WriteLog("timer event called..");
                dbConnect();
            }
            catch (Exception ex)
            {
                EventViewer.WriteLog("Exception at DispatcherTimer_Tick Msg-" + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
            }
            //if (stop) return;
            //timer.Change(30000, Timeout.Infinite); // 30 seconds
        }

        private static void dbConnect()
        {
            _dbContentSearch.getOpenItems();
        }

        public void start()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            EventViewer.WriteLog("Service Started");
            _dbContentSearch = new DBOfficeFileSearch();
            timerStart();
        }

        protected override void OnStop()
        {
            Global.timer.Stop();
            Global.timer.Dispose();
            EventViewer.WriteLog("Service Stopped");
        }
    }
}
