﻿namespace mhiextractionService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.alfaDOCKSetupSheetserviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.alfaDOCKSetupSheetServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // alfaDOCKSetupSheetserviceProcessInstaller
            // 
            this.alfaDOCKSetupSheetserviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.alfaDOCKSetupSheetserviceProcessInstaller.Password = null;
            this.alfaDOCKSetupSheetserviceProcessInstaller.Username = null;
            this.alfaDOCKSetupSheetserviceProcessInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.alfaDOCKSetupSheetserviceProcessInstaller_AfterInstall);
            // 
            // alfaDOCKSetupSheetServiceInstaller
            // 
            this.alfaDOCKSetupSheetServiceInstaller.Description = "MHI SheetExtraction Service ";
            this.alfaDOCKSetupSheetServiceInstaller.DisplayName = "MHI SheetExtraction Service ";
            this.alfaDOCKSetupSheetServiceInstaller.ServiceName = "mhi-extractionservice";
            this.alfaDOCKSetupSheetServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.alfaDOCKSetupSheetserviceProcessInstaller,
            this.alfaDOCKSetupSheetServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller alfaDOCKSetupSheetserviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller alfaDOCKSetupSheetServiceInstaller;
    }
}