﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mhiextractionService
{
    class DBOfficeFileSearch
    {

       
        static List<string> getFileIds()
        {
            String sqlFormattedDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            string file_id_log = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar + "fileid_log_" + sqlFormattedDate + ".txt";
            List<string> lines=new List<string>();
            using (StreamReader sr = File.OpenText(file_id_log))
            {
                string[] lines_arr = File.ReadAllLines(file_id_log);
                for (int x = 0; x <= lines_arr.Length - 1; x++)
                {
                    lines.Add(lines_arr[x]);
                }
               
            }
            return lines;
        }
        Boolean isFileIdExists (List<string> ids, string id )
        {
            Boolean isMatch = false;
            if (ids != null && ids.Count>0)
            {
                isMatch = ids.Any(s => s.Contains(id));
            }
                return isMatch;
        }
        public void getOpenItems()
        {
            if (Global.timer.Enabled)
                Global.timer.Stop();

            String yesdayDate = DateTime.UtcNow.Date.AddDays(-1).ToString("yyyy-MM-dd");
            string Yesdayfile_path = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar + "fileid_log_" + yesdayDate + ".txt";
            if (File.Exists(Yesdayfile_path))
            {
                File.Delete(Yesdayfile_path);
            }
                String sqlFormattedDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            EventViewer.WriteFileLog("");

            List<string> fIds = getFileIds();
            FileInfo fInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar +"log_content.txt");
            if (fInfo.Length > 2147483248)
            {
                File.WriteAllText(fInfo.FullName, "");
            }

            //clear log file

            FileInfo fInfo_log = new FileInfo(@"F:\xampp\htdocs\alfadockpro\factory_layout\1\gpnapilog-mhi.txt");
            if (fInfo_log.Length > 2147483248)
            {
                File.WriteAllText(fInfo_log.FullName, "");
            }
            EventViewer.WriteLog("fetching db");

            using (var con = new SqlConnection(DBContext.ConnectionString))
            {
                try
                {
                    QueueManager queueInstance = QueueManager.Instance;
                    EventViewer.WriteLog("current items count : " + queueInstance.getItemsCount());

                    con.Open();

                   // sqlFormattedDate = "2022-07-04";
                    string query = @"SELECT Id,guid,compid,filename,date,thumbguid from SocketFiles with(nolock) WHERE deleted = 0 AND type='s08' AND compid IN(841) AND cast(modifieddate as date) = '" + sqlFormattedDate + "' ORDER BY Id DESC";

                    //string query = @"SELECT Id,guid,compid,filename,date,thumbguid from SocketFiles with(nolock) WHERE deleted = 0 AND type='s08' AND compid IN(841) AND (Id=37067347)";

                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        int count = 0;
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Upload.data data = new Upload.data();

                                if (!reader.IsDBNull(reader.GetOrdinal("guid")))
                                    data.guid = reader["guid"].ToString();

                                if (!reader.IsDBNull(reader.GetOrdinal("thumbguid")))
                                    data.thumbguid = reader["thumbguid"].ToString();

                                if (!reader.IsDBNull(reader.GetOrdinal("date")))
                                {
                                    string datestr = reader.GetDateTime(reader.GetOrdinal("date")).ToString();
                                    data.up_date = DateTime.Parse(datestr).Date.ToString("yyyy-MM-dd");
                                }

                                if (!reader.IsDBNull(reader.GetOrdinal("compid")))
                                    data.compid = reader["compid"].ToString();

                                if (!reader.IsDBNull(reader.GetOrdinal("filename")))
                                    data.filename = reader["filename"].ToString();
                                

                                if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                                    data.fileid = (Int32)reader["Id"];
                             string fileId_val= reader["Id"].ToString();
                                data.type = "socket";

                                //  if (!reader.IsDBNull(reader.GetOrdinal("subtype")))
                                data.subtype = "s08";

                                 if (!isFileIdExists(fIds,fileId_val) && data.guid!=null)
                                        queueInstance.addItem(data);
                                
                                
                                count++;
                            }
                            EventViewer.WriteLog("db items- "+ count.ToString() + "--total queue items now  " + queueInstance.getItemsCount());
                        }
                        if (queueInstance.getItemsCount() <= 0)
                        {
                            EventViewer.WriteLog("db items- " + count.ToString() + "--total db items now  " + queueInstance.getItemsCount());

                            if (!Global.timer.Enabled)
                                Global.timer.Enabled = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (!Global.timer.Enabled)
                        Global.timer.Enabled = true;
                    EventViewer.WriteLog("Exception in getOpenItems - Msg -" + ex.Message + Environment.NewLine + "Stacktrace -" + ex.StackTrace);

                }
                finally
                {
                    if (con != null) con.Close();
                }
            }
        }
    }
}
