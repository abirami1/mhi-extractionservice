﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace mhiextractionService
{
    class Upload
    {
        
        [DataContract]
        public class record
        {
            public record()
            {
            }

            [DataMember]
            public string type { get; set; }

            [DataMember]
            public string id { get; set; }

            [DataMember]
            public data fields;
        }

        [DataContract]
        public class data
        {
            [DataMember]
            public string compid { get; set; }
            [DataMember]
            public int fileid { get; set; }
            [DataMember]
            public string guid { get; set; }
            [DataMember]
            public string filename { get; set; }
            [DataMember]
            public string thumbguid { get; set; }
            [DataMember]
            public string up_date { get; set; }

            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string subtype { get; set; }
           
        }

       

        
    }
}
