﻿using Amazon.S3;
using System;
using System.Threading.Tasks;
using Amazon.S3.Transfer;
using System.IO;
using Amazon.S3.Model;

namespace mhiextractionService
{
    public class S3Storage
    {
        public static string FilesBucket
        {
            get
            {


                //return "sirpi";
               return "alfadock-mhi/alfadock"; //prod
                //return "atkg-test"; //dev

            }
        }
        
        public S3Storage()
        {
            //TODO:remove this MHI
            string keyid = "AKIARXEHOJBO43XY73RG";
            string key = "xhV+tahpqMKPnuQXf3BJ++4TmV+I/NxXSr55eDnE";
            string region = "ap-northeast-1";

            AWS_ACCESS_KEY_ID = keyid;
            AWS_SECRET_ACCESS_KEY = key;
            AWS_REGION = region;

            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", AWS_ACCESS_KEY_ID);
            Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", AWS_SECRET_ACCESS_KEY);
            Environment.SetEnvironmentVariable("AWS_REGION", AWS_REGION);
           
           //Dock
            /*string keyid = "AKIAIXYF6WANSYIGRA4A";
            string key = "pDl1//tDjjTLhJWThMf0bWr2FmIfW8XRBC1UCmnX";
            string region = "ap-northeast-1";


            this.AWS_ACCESS_KEY_ID = keyid;
            this.AWS_SECRET_ACCESS_KEY = key;
            this.AWS_REGION = region;

            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", AWS_ACCESS_KEY_ID);
            Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", AWS_SECRET_ACCESS_KEY);
            Environment.SetEnvironmentVariable("AWS_REGION", AWS_REGION);
            */
        }

        public string AWS_ACCESS_KEY_ID { get; private set; }
        public string AWS_SECRET_ACCESS_KEY { get; private set; }
        public string AWS_REGION { get; private set; }



        public async Task UploadFile(string bucketName, string filePath, string fileName)
        {
            using (IAmazonS3 client = new AmazonS3Client())
            {
                var transferUtility = new TransferUtility(client);

                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest()
                {
                    BucketName = bucketName,
                    FilePath = filePath,
                    Key = fileName

                };

                await transferUtility.UploadAsync(request);
            }
        }

        public async Task DownloadFile(string bucketName, string filename, string filepath)
        {
            using (IAmazonS3 client = new AmazonS3Client())
            {
                var transferUtility = new TransferUtility(client);

                TransferUtilityDownloadRequest request = new TransferUtilityDownloadRequest()
                {
                    BucketName = bucketName,
                    FilePath = filepath,
                    Key = filename
                };

                await transferUtility.DownloadAsync(request);
            }
        }


        public async Task UploadFileAsStream(string bucketName, string fileName, Stream stream)
        {
            using (IAmazonS3 client = new AmazonS3Client())
            {
                try
                {
                    var transferUtility = new TransferUtility(client);

                    await transferUtility.UploadAsync(stream, bucketName, fileName);
                }
                catch(Exception ex)
                {

                }
            }
        }

        public async Task<Stream> DownloadFileAsStream(string bucketname,string filename)
        {
            using (IAmazonS3 client = new AmazonS3Client())
            {
                var transferUtility = new TransferUtility(client);

                try
                {
                    var task = await transferUtility.OpenStreamAsync(bucketname, filename);
                    return task;
                }
                catch(Exception ex)
                {

                }

                return null;
            }
        }

        public string GetLink(string bucketname,string filename)
        {
            using (IAmazonS3 s3Client = new AmazonS3Client())
            {
                GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest()
                {
                    BucketName = bucketname,
                    Key = filename,
                    Expires = DateTime.Now.AddYears(20)
                };

                string url = s3Client.GetPreSignedURL(request1);
                return url;
            }            
        }
    }
}

