﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text;
using iTextSharp.text.pdf;
using Ghostscript.NET;
using System.Drawing;
using Newtonsoft.Json.Linq;
using System.Drawing.Imaging;
using System.Net;
using System.Globalization;

namespace mhiextractionService
{
    class QueueManager
    {
       
        private static ConcurrentDictionary<string, Upload.data> dictFiles = null;
        private static QueueManager instance = null;
        private static readonly object padlock = new object();
        private static Thread uploaderThread;
        private static S3Storage _s3;
        public static Boolean isMultiple = false;
        public static Boolean isBlank = false;
        private static DBOfficeFileSearch _dbContentSearch;
        public static Boolean isedge_multi = false;

        static string c_date = DateTime.Now.ToString("yyyy-MM-dd");
        static string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
        static MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
        static string st_key = "sfGa0kl7lO9fXWaE1rENp";
        static string dx_key = "7aBNl26gf0yaxEDrFJpE";
        static string id_4241 ="";
        static string id_4170 = "";
        static string id_1710 = "";
        static string id_4000 = "";

        public static QueueManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new QueueManager();
                            dictFiles = new ConcurrentDictionary<string, Upload.data>();
                            _s3 = new S3Storage();
                            _dbContentSearch = new DBOfficeFileSearch();
                            if (uploaderThread == null)
                            {
                                uploaderThread = new Thread(new ThreadStart(upload));
                                uploaderThread.Start();
                            }
                        }
                    }
                }

                return instance;
            }

        }

        private async static void upload()
        {
            while (true)
            {
                try
                {
                    if (dictFiles.Any())
                    {
                        foreach (var item in dictFiles.ToArray<KeyValuePair<string, Upload.data>>())
                        {
                            try
                            {
                               
                                await uploadContents(item);
                            }
                            catch (Exception ex)
                            {
                                EventViewer.WriteLog("upload crash -" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                            }

                        }
                        continue;
                    }
                    Thread.Sleep(120000);
                }
                catch (Exception e)
                {
                    EventViewer.WriteLog("upload crash -" + Environment.NewLine + e.Message + Environment.NewLine + e.StackTrace);

                }
            }

        }
        public static void extract_process_info()
        {
            string html = string.Empty;

            string pr_id_url = "https://" + DBContext.getDomainName + "/api/get_alfaerp_settings/SCHEDULER?key=56ACtlLKKf8LasERZPyf&factory=hq";
            //EventViewer.WriteLog(pr_id_url);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(pr_id_url);
            request1.AutomaticDecompression = DecompressionMethods.GZip;
            using (HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse())
            using (Stream stream1 = response1.GetResponseStream())
            using (StreamReader reader1 = new StreamReader(stream1))
            {
                html = reader1.ReadToEnd();
                dynamic val_response1 = JObject.Parse(html);

                if (val_response1.processes != null)
                {
                    for (int y = 0; y < val_response1.processes.Count; y++)
                    {
                        string processname = val_response1.processes[y].name;
                        if (processname == "4170")
                        {
                            id_4170 = val_response1.processes[y].id;
                        }
                        else if (processname == "4241")
                        {
                            id_4241 = val_response1.processes[y].id;
                        }
                        else if (processname == "1710")
                        {
                            id_1710 = val_response1.processes[y].id;
                        }
                        else if (processname == "4000")
                        {
                            id_4000 = val_response1.processes[y].id;
                        }
                    }
                }
            }

                        }
        public static void extract_gnn_data_mhi(string path, MySqlConnection mcon2, KeyValuePair<String, Upload.data> item)
        {
            int f_error = 0;
            try
            {
                using (StreamReader csv1 = new StreamReader(File.OpenRead(path)))
                {
                    int counter = 0;
                    while (!csv1.EndOfStream)
                    {
                        counter++;
                        var line = csv1.ReadLine();
                        var csv = line.Split(',');
                        if (counter > 1)
                        {
                            string serialno = csv[0];
                            string pgmno = csv[2];
                            string matcode = csv[4];
                            string filename = csv[5];
                            string moid = csv[10];
                            string partno = csv[9];
                            string count_str = csv[11];

                            string len_str = csv[12];
                            string width_str = csv[13];
                            string rem_str = csv[14];

                            int tot_count = 0;
                            if (!string.IsNullOrEmpty(count_str))
                            {
                                tot_count = Convert.ToInt32(count_str);
                            }

                            float len_count = 0;
                            if (!string.IsNullOrEmpty(len_str))
                            {
                                len_count = float.Parse(len_str);
                            }
                            float width_count = 0;
                            if (!string.IsNullOrEmpty(width_str))
                            {
                                width_count = float.Parse(width_str);
                            }
                            float mat_field = 0;
                            if (len_count != 0 || width_count != 0)
                            {
                                mat_field = len_count * width_count;
                            }
                            float rem_count = 0;
                            if (!string.IsNullOrEmpty(rem_str))
                            {
                                rem_count = float.Parse(rem_str);
                            }
                            string mlc = csv[6];
                            string yield_val = csv[8];
                            if (csv[8].Contains("."))
                            {
                                decimal yield = Math.Round(Convert.ToDecimal(csv[8]));
                                yield_val = "" + yield;
                            }

                            string pr_sec = csv[7];
                            string processtime = "0:0:0";
                            if (!string.IsNullOrEmpty(pr_sec))
                            {
                                int process_seconds = Convert.ToInt32(pr_sec);

                                TimeSpan timespan = TimeSpan.FromSeconds(process_seconds);
                                int hour = timespan.Hours;
                                int min = timespan.Minutes;
                                int sec = timespan.Seconds;

                                processtime = hour + ":" + min + ":" + sec;
                            }
                            //EventViewer.WriteLog(partno);
                            string partLength = "0";
                            string partWidth = "0";
                            string partThickness = "0.00";
                            string smimodel = "";
                            string material = "";
                            string orderid = "0";
                            DateTime date = DateTime.Parse(c_date);
                            string fromdate = date.AddDays(-300).ToString("yyyy-MM-dd");
                            string todate = date.AddDays(100).ToString("yyyy-MM-dd");
                            string duedate = date.ToString("yyyy-MM-dd");
                            string intduedate = date.ToString("yyyy-MM-dd");
                            string jobduedate = "";
                            string final_jobdate = date.ToString("yyyy-MM-dd");
                            string final_intdate = date.ToString("yyyy-MM-dd");
                            string matwidth = "";
                            string matlength = "";
                            string html = string.Empty;
                            
                            
                                            string url = @"https://" + DBContext.getDomainName + "/api/get_alfaerp_data/SCHEDULERORDERV2?key=" + st_key + "&pids=" + id_4170 + "&page=0&limit=100&ps=-1,0,4,1,2,3&sv="+moid+"&ds=" + fromdate + "&de=" + todate + "&df==deliveryDate,processDate";
                                            //EventViewer.WriteLog(url);
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                                            request.AutomaticDecompression = DecompressionMethods.GZip;

                                            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                                            using (Stream stream = response.GetResponseStream())
                                            using (StreamReader reader = new StreamReader(stream))
                                            {
                                                html = reader.ReadToEnd();

                                                dynamic val_response = JObject.Parse(html);
                                                int tot_count_val = val_response.count;
                                                for (int i = 0; i < tot_count_val; i++)
                                                {
                                                    //var cust_name = val_response.mos[i].customerName;
                                                    // if (cust_name == "MHI-VA") {
                                                    for (int j = 0; j < val_response.mos[i].itemList.Count; j++)
                                                    {
                                                        string prodno = val_response.mos[i].itemList[j].productNo;
                                                        string mono = val_response.mos[i].itemList[j].smi_mo_number;
                                       
                                                    if (prodno == partno && mono == moid)
                                                        {
                                                            partLength = val_response.mos[i].itemList[j].partLength;
                                                            partWidth = val_response.mos[i].itemList[j].partWidth;
                                                            partThickness = val_response.mos[i].itemList[j].partThickness;
                                                            smimodel = val_response.mos[i].itemList[j].smi_model_number;
                                                            material = val_response.mos[i].itemList[j].smi_alloy;
                                                            matwidth = val_response.mos[i].itemList[j].materialWidth;
                                                            matlength = val_response.mos[i].itemList[j].materialLength;
                                                            duedate = val_response.mos[i].itemList[j].dueDate;
                                                            intduedate = val_response.mos[i].itemList[j].internal_due_date;
                                                            //status = val_response.mos[i].itemList[j].processList[0].status;
                                                            jobduedate = duedate;
                                                            final_jobdate = duedate;
                                                            final_intdate = duedate;

                                                            if (!String.IsNullOrEmpty(intduedate))
                                                            {
                                                                DateTime temp = DateTime.ParseExact(intduedate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                                                                final_intdate = temp.ToString("yyyy-MM-dd");
                                                            }
                                                            if (val_response.mos[i].itemList[j].processList.Count > 0)
                                                            {
                                                                orderid = val_response.mos[i].itemList[j].processList[0].id;
                                                                jobduedate = val_response.mos[i].itemList[j].processList[0].endDate;
                                                                if (!String.IsNullOrEmpty(jobduedate))
                                                                {
                                                                    DateTime temp = DateTime.ParseExact(jobduedate, "MM/dd/yy HH:mm:ss tt", CultureInfo.InvariantCulture);
                                                                    final_jobdate = temp.ToString("yyyy-MM-dd");
                                                                }
                                                            }
                                                            int rem_val = 1;
                                                            if (rem_count <= 0)
                                                            {
                                                                rem_val = 0;
                                                            }
                                            string CmdText = "INSERT IGNORE INTO setupsheetmhi1 VALUES(@sid,@machinename,@materialname,@materialsize,@filename,@date,@processed,@sheets,@processingtime,@starttime,@endtime,@programname,@pduedate,@intduedate,@qty,@partnum,@compid,@partname,@comment,@orderno,@sheetcode,@toolinfo,@serialno,@priority,@pdfdownloaded,@material,@partthickness,@partwidth,@partlength,@smimodel,@orderid,@suspendval)";
                                            MySqlCommand cmd = new MySqlCommand(CmdText, mcon2);
                                            cmd.Parameters.AddWithValue("@sid", pgmno + "p" + moid + "m" + counter + "-" + item.Value.fileid);
                                            cmd.Parameters.AddWithValue("@machinename", "FLC3015AJ");
                                            cmd.Parameters.AddWithValue("@materialname", matcode);
                                            cmd.Parameters.AddWithValue("@materialsize", matwidth + "x" + matlength);
                                            cmd.Parameters.AddWithValue("@filename", filename);
                                            cmd.Parameters.AddWithValue("@sheets", 1);
                                            cmd.Parameters.AddWithValue("@processingtime", processtime);
                                            cmd.Parameters.AddWithValue("@starttime", "0000-00-00 00:00:00");
                                            cmd.Parameters.AddWithValue("@endtime", "0000-00-00 00:00:00");
                                            cmd.Parameters.AddWithValue("@comment", mlc);
                                            cmd.Parameters.AddWithValue("@programname", Encoding.UTF8.GetBytes(pgmno));
                                            cmd.Parameters.AddWithValue("@partnum", partno);
                                            cmd.Parameters.AddWithValue("@partname", yield_val);
                                            cmd.Parameters.AddWithValue("@qty", tot_count);
                                            cmd.Parameters.AddWithValue("@orderno", moid);
                                            cmd.Parameters.AddWithValue("@compid", "841");
                                            cmd.Parameters.AddWithValue("@processed", 0);
                                            cmd.Parameters.AddWithValue("@pduedate", final_jobdate);
                                            cmd.Parameters.AddWithValue("@intduedate", final_intdate);
                                            cmd.Parameters.AddWithValue("@date", item.Value.up_date);
                                            cmd.Parameters.AddWithValue("@sheetcode", "");
                                            cmd.Parameters.AddWithValue("@toolinfo", "");
                                            cmd.Parameters.AddWithValue("@serialno", serialno);
                                            cmd.Parameters.AddWithValue("@priority", 0);
                                            cmd.Parameters.AddWithValue("@pdfdownloaded", 1);
                                            cmd.Parameters.AddWithValue("@material", material);
                                            cmd.Parameters.AddWithValue("@partthickness", partThickness);
                                            cmd.Parameters.AddWithValue("@partwidth", partWidth);
                                            cmd.Parameters.AddWithValue("@partlength", partLength);
                                            cmd.Parameters.AddWithValue("@smimodel", smimodel);
                                            cmd.Parameters.AddWithValue("@orderid", orderid);
                                            cmd.Parameters.AddWithValue("@suspendval", 0);
                                            int result = cmd.ExecuteNonQuery();
                                            if (result != 1)
                                            {
                                                string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetmhi1 SET material='{1}',partthickness='{2}',partwidth='{3}',partlength='{4}',smimodel='{5}',orderid='{7}' where programname='{0}' AND orderno='{6}'", pgmno, material, partThickness, partWidth, partLength, smimodel, moid, orderid);


                                                using (MySqlCommand command = new MySqlCommand(query, mcon2))
                                                {
                                                    int row = command.ExecuteNonQuery();
                                                    if (row == 1)
                                                    {
                                                        EventViewer.WriteLog("Updated-" + pgmno);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                EventViewer.WriteLog(serialno + "->" + matcode + "->" + mat_field + "->" + rem_count + "->" + moid + "->" + rem_val);

                                                exnest_api_call(serialno, matcode, "" + mat_field, "" + rem_count, moid, rem_val);
                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        
                           

                        }
                    }

                }
            }
            catch (Exception e)
            {
                EventViewer.WriteLog(e);
                if (e.ToString().Contains("Fatal"))
                {
                    f_error = 1;
                }
            }
            finally
            {
                if (f_error == 0)
                {
                    EventViewer.WriteFileLog("" + item.Value.fileid);
                }
                if (File.Exists(path))
                    File.Delete(path);
            }
        }

        public static void csmupdate_api_call(string msc, string monumber)
        {
            try
            {
                string url_csmupdate = "https://" + DBContext.getDomainName + "/api/set_alfaerp_data/CSMSTATEUPDATE?key=7aBNl26gf0yaxEDrFJpE&materialserialcode=" + msc + "&monumber=" + monumber;
                string result = Get_req(url_csmupdate);
                EventViewer.WriteLog("CSM Update: "+monumber + "->"+ msc+"->" + result);
            }
            catch (Exception e)
            {
                EventViewer.WriteLog("CSMUPDATE Log" + e);
            }
        }

        public static void exnest_api_call(string msc, string mc, string materialyfield, string remenantarea, string monumber, int remval)
        {try
            {
                
                /*
                WebClient webClient = new WebClient();
                webClient.QueryString.Add("key", "7aBNl26gf0yaxEDrFJpE");
                webClient.QueryString.Add("materialserialcode", msc);
                webClient.QueryString.Add("materialcode", mc);
                webClient.QueryString.Add("materialyfield", materialyfield);
                webClient.QueryString.Add("isremnnant", "" + remval);
                webClient.QueryString.Add("remnnantarea", remenantarea);
                webClient.QueryString.Add("monumber", monumber);
                EventViewer.WriteLog("https://" + DBContext.getDomainName + "/api/set_alfaerp_data/ALFADEXUPDATE");

                string result = webClient.DownloadString("https://" + DBContext.getDomainName + "/api/set_alfaerp_data/ALFADEXUPDATE");
                */
                
                string url_dexupdate = "https://" + DBContext.getDomainName + "/api/set_alfaerp_data/ALFADEXUPDATE?key=7aBNl26gf0yaxEDrFJpE&materialserialcode=" + msc + "&materialcode="+mc+"&materialyfield="+ materialyfield + "&isremnnant="+ remval +"&remnantarea="+ remenantarea + "&monumber=" + monumber;
                //EventViewer.WriteLog(url_dexupdate);
                string result = Get_req(url_dexupdate);
                EventViewer.WriteLog(monumber + "->" + result);

            }
            catch(Exception e)
            {
                EventViewer.WriteLog("DEXUPDATE Log" + e);
            }
        }
        public static string Get_req(string uri)
        {
            string responseBody = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.Method = "GET";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                var stream = response.GetResponseStream();
                responseBody = "";
                using (var reader = new StreamReader(stream))
                {
                    responseBody = reader.ReadToEnd();
                    reader.Close();
                }
                response.Close();
                response.Dispose();
                responseBody = responseBody.ToLower();


            }

            return responseBody;
    }
        public static void update_gnn_mo_id(MySqlConnection mcon2)
        {
            DateTime date = DateTime.Parse(c_date);

            string fromdate = date.AddDays(-300).ToString("yyyy-MM-dd");
            string todate = date.AddDays(100).ToString("yyyy-MM-dd");

            string html = string.Empty;
            string url = @"https://" + DBContext.getDomainName + "/api/get_alfaerp_data/SCHEDULERORDERV2?key=" + st_key + "&pids=" + id_1710 + "&page=0&limit=100&ps=-1,0,4,1,2,3&sv=&ds=" + fromdate + "&de=" + todate + "&df==deliveryDate,processDate";
            //EventViewer.WriteLog(url);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();

                dynamic val_response = JObject.Parse(html);
                int tot_count_val = val_response.count;
                EventViewer.WriteLog("" + tot_count_val);
                for (int i = 0; i < tot_count_val; i++)
                {
                   // var cust_name = val_response.mos[i].customerName;
                    var mo_wo_id = val_response.mos[i].id;
                    for (int j = 0; j < val_response.mos[i].itemList.Count; j++)
                    {
                        string prodno = val_response.mos[i].itemList[j].productNo;
                        string mono = val_response.mos[i].itemList[j].smi_mo_number;
                        string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetmhi3 SET mo_wo_id='{1}' where (mo_wo_id is NULL) AND orderno='{0}'", mono, mo_wo_id);


                        using (MySqlCommand command = new MySqlCommand(query, mcon2))
                        {
                            int row = command.ExecuteNonQuery();
                            
                        }

                    }
                }
                }

        


                }
        public static void extract_gnn_çut_data_mhi(string path, MySqlConnection mcon2, KeyValuePair<String, Upload.data> item)
        {
            int f_error = 0;
            try
            {
                int num_lines = 0; int csm_lines = 0;
                using (StreamReader csv2 = new StreamReader(path))
                {
                    while (csv2.ReadLine()!=null)
                    {
                        num_lines++;
                    }
                    EventViewer.WriteLog("Lines-" + num_lines);
                    csv2.Close();
                }
                using (StreamReader csv3 = new StreamReader(path))
                {
                    string temp_serial_val = "";
                    string temp_moid_val = "";
                    while (!csv3.EndOfStream)
                    {
                        csm_lines++;
                        var line = csv3.ReadLine();
                        var csv = line.Split(',');
                        if (csm_lines > 1)
                        {
                            string scode = csv[0];
                            string moid = csv[2];
                            if (String.IsNullOrEmpty(temp_moid_val) && (csm_lines == num_lines))
                            {
                                csmupdate_api_call(scode, moid);
                            }
                            if (temp_moid_val!="" && temp_moid_val.Equals(moid))
                            {
                                temp_serial_val = temp_serial_val+","+scode;
                                if (csm_lines == num_lines)
                                {
                                    csmupdate_api_call(temp_serial_val, moid);
                                }
                            }
                            else if(temp_moid_val != "" && !temp_moid_val.Equals(moid)){
                                
                                csmupdate_api_call(temp_serial_val, temp_moid_val);
                                if(csm_lines == num_lines)
                                {
                                    csmupdate_api_call(scode, moid);
                                }
                               
                            }

                            temp_serial_val = scode;
                            temp_moid_val = moid;
                        }
                    }
                    csv3.Close();
                }
               using (StreamReader csv1 = new StreamReader(File.OpenRead(path)))
                {
                    int counter = 0;
                   
                    Boolean isfirstformat = true;
                    string temp_idval = "";
                    string temp_serial_val = "";
                    string temp_moid_val = "";
                   
                        while (!csv1.EndOfStream)
                    {
                        counter++;
                        var line = csv1.ReadLine();
                        var csv = line.Split(',');
                        if (counter == 1)
                        {
                            string moid = csv[2];
                            if(moid=="MO ID")
                            {
                                isfirstformat = false; 
                            }
                        }
                        if (counter > 1)
                        {
                            string scode = csv[0];
                            string moid = csv[2];
                            string count_str = csv[3];
                            string length_str = csv[4];
                            string rem_length_str = csv[5];
                            string partno = csv[6];

                            if (isfirstformat)
                            {
                                partno = csv[2];
                                moid = csv[3];
                                count_str = csv[4];
                                length_str = csv[5];
                                rem_length_str = csv[6];

                            }
                            int tot_count = 0;
                            if (!string.IsNullOrEmpty(count_str))
                            {
                                tot_count = Convert.ToInt32(count_str);
                            }
                            float flt1 = 0;
                            float flt2 = 0;
                            if (length_str != "")
                            {
                                flt1 = float.Parse(length_str);
                            }

                            if (rem_length_str != "")
                            {
                                flt2 = float.Parse(rem_length_str);
                            }
                            else
                            {
                                rem_length_str = "0";
                            }

                            float flt3 = flt1 - flt2;
                            DateTime date = DateTime.Parse(c_date);
                            string fromdate = date.AddDays(-300).ToString("yyyy-MM-dd");
                            string todate = date.AddDays(100).ToString("yyyy-MM-dd");

                            //EventViewer.WriteLog("cutinfo:"+partno);
                            string partLength = "0";
                            string partWidth = "0";
                            string partThickness = "0.00";
                            string smimodel = "";
                            string material = "";
                            string shape = "";
                            string orderid = "0";
                            string duedate = date.ToString("yyyy-MM-dd");
                            string intduedate = date.ToString("yyyy-MM-dd");
                            string matwidth = "";
                            string matlength = "";
                            string matcode = "";
                            //string serialcode = "";
                            string matcode_val = "";
                            //string status = "";
                            string jobduedate = "";
                            string final_jobdate = date.ToString("yyyy-MM-dd");
                            string final_intdate = date.ToString("yyyy-MM-dd");

                            string html = string.Empty;

                            
                            string url = @"https://" + DBContext.getDomainName + "/api/get_alfaerp_data/SCHEDULERORDERV2?key=" + st_key + "&pids="+ id_1710 + "&page=0&limit=100&ps=-1,0,4,1,2,3&sv="+moid+"&ds=" + fromdate + "&de=" + todate + "&df==deliveryDate,processDate";
                            //EventViewer.WriteLog(url);

                            //EventViewer.WriteLog(partno);
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                            request.AutomaticDecompression = DecompressionMethods.GZip;

                            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                            using (Stream stream = response.GetResponseStream())
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                html = reader.ReadToEnd();

                                dynamic val_response = JObject.Parse(html);
                                int tot_count_val = val_response.count;
                                //EventViewer.WriteLog("" + tot_count_val);
                                for (int i = 0; i < tot_count_val; i++)
                                {
                                    // var cust_name = val_response.mos[i].customerName;
                                    var mo_wo_id = val_response.mos[i].id;
                                    // if (cust_name == "MHI-VA") {
                                    for (int j = 0; j < val_response.mos[i].itemList.Count; j++)
                                    {
                                        string prodno = val_response.mos[i].itemList[j].productNo;
                                        string mono = val_response.mos[i].itemList[j].smi_mo_number;
                                        if (prodno == partno && mono == moid)
                                        {
                                            partLength = val_response.mos[i].itemList[j].partLength;
                                            partWidth = val_response.mos[i].itemList[j].partWidth;
                                            partThickness = val_response.mos[i].itemList[j].partThickness;
                                            smimodel = val_response.mos[i].itemList[j].smi_model_number;
                                            material = val_response.mos[i].itemList[j].smi_alloy;
                                            matcode_val = val_response.mos[i].itemList[j].materialCode;
                                            duedate = val_response.mos[i].itemList[j].dueDate;
                                            intduedate = val_response.mos[i].itemList[j].internal_due_date;
                                            matwidth = val_response.mos[i].itemList[j].materialWidth;
                                            matlength = val_response.mos[i].itemList[j].materialLength;
                                            shape = val_response.mos[i].itemList[j].smi_shape;
                                            //matcode = val_response.mos[i].itemList[j].materialCode2;
                                            //serialcode = val_response.mos[i].itemList[j].sheetSerialCode[0];
                                            //matcode_val = val_response.mos[i].itemList[j].materialCode;
                                            //status = val_response.mos[i].itemList[j].processList[0].status;
                                            jobduedate = duedate;
                                            final_jobdate = duedate;
                                            final_intdate = duedate;
                                            if (!String.IsNullOrEmpty(material))
                                            {
                                                if (!String.IsNullOrEmpty(intduedate))
                                                {
                                                    DateTime temp = DateTime.ParseExact(intduedate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                                                    final_intdate = temp.ToString("yyyy-MM-dd");
                                                }
                                                if (val_response.mos[i].itemList[j].processList.Count > 0)
                                                {
                                                    jobduedate = val_response.mos[i].itemList[j].processList[0].endDate;
                                                    orderid = val_response.mos[i].itemList[j].processList[0].id;
                                                    if (!String.IsNullOrEmpty(jobduedate))
                                                    {
                                                        DateTime temp = DateTime.ParseExact(jobduedate, "MM/dd/yy HH:mm:ss tt", CultureInfo.InvariantCulture);
                                                        final_jobdate = temp.ToString("yyyy-MM-dd");
                                                    }
                                                }


                                                //EventViewer.WriteLog(material);
                                                string CmdText1 = "INSERT IGNORE INTO setupsheetmhi3 VALUES(@sid,@machinename,@materialname,@materialsize,@filename,@date,@processed,@sheets,@processingtime,@starttime,@endtime,@programname,@pduedate,@intduedate,@qty,@partnum,@compid,@partname,@comment,@orderno,@sheetcode,@toolinfo,@serialno,@priority,@pdfdownloaded,@partthickness,@partwidth,@partlength,@smimodel,@orderid,@suspendval,@shape,@mo_wo_id)";
                                                MySqlCommand cmd = new MySqlCommand(CmdText1, mcon2);
                                                cmd.Parameters.AddWithValue("@sid", item.Value.fileid + "-" + moid + "$" + partno);
                                                cmd.Parameters.AddWithValue("@machinename", "FLC3015AJ");
                                                cmd.Parameters.AddWithValue("@materialname", material);
                                                cmd.Parameters.AddWithValue("@materialsize", matwidth + "x" + matlength);
                                                cmd.Parameters.AddWithValue("@filename", "");
                                                cmd.Parameters.AddWithValue("@sheets", 1);
                                                cmd.Parameters.AddWithValue("@processingtime", "");
                                                cmd.Parameters.AddWithValue("@starttime", "0000-00-00 00:00:00");
                                                cmd.Parameters.AddWithValue("@endtime", "0000-00-00 00:00:00");
                                                cmd.Parameters.AddWithValue("@comment", matcode_val);
                                                cmd.Parameters.AddWithValue("@programname", moid);
                                                cmd.Parameters.AddWithValue("@partnum", partno);
                                                cmd.Parameters.AddWithValue("@partname", "");
                                                cmd.Parameters.AddWithValue("@qty", tot_count);
                                                cmd.Parameters.AddWithValue("@orderno", moid);
                                                cmd.Parameters.AddWithValue("@compid", "841");
                                                cmd.Parameters.AddWithValue("@processed", 0);
                                                cmd.Parameters.AddWithValue("@pduedate", final_jobdate);
                                                cmd.Parameters.AddWithValue("@intduedate", final_intdate);
                                                cmd.Parameters.AddWithValue("@date", c_date);
                                                cmd.Parameters.AddWithValue("@sheetcode", "");
                                                cmd.Parameters.AddWithValue("@toolinfo", "");
                                                cmd.Parameters.AddWithValue("@serialno", scode);
                                                cmd.Parameters.AddWithValue("@priority", 0);
                                                cmd.Parameters.AddWithValue("@pdfdownloaded", 1);
                                                cmd.Parameters.AddWithValue("@partthickness", partThickness);
                                                cmd.Parameters.AddWithValue("@partwidth", partWidth);
                                                cmd.Parameters.AddWithValue("@partlength", partLength);
                                                cmd.Parameters.AddWithValue("@smimodel", smimodel);
                                                cmd.Parameters.AddWithValue("@orderid", orderid);
                                                cmd.Parameters.AddWithValue("@suspendval", 0);
                                                cmd.Parameters.AddWithValue("@shape", shape);
                                                cmd.Parameters.AddWithValue("@mo_wo_id", mo_wo_id);

                                                //EventViewer.WriteLog("Shape-"+shape);
                                                int result = cmd.ExecuteNonQuery();
                                                string org_idval = moid + "$" + partno;
                                                if (result == 1)
                                                {
                                                    temp_idval = moid + "$" + partno;
                                                   
                                                    temp_serial_val = scode;
                                                    temp_moid_val = moid;
                                                   
                                                    int rem_val = 1;
                                                    if (rem_length_str.Equals("0"))
                                                    {
                                                        rem_val = 0;
                                                    }
                                                    if (rem_val == 1)
                                                    {
                                                        EventViewer.WriteLog(scode + "->" + matcode_val + "->" + flt3 + "->" + rem_length_str + "->" + moid + "->" + rem_val);
                                                        exnest_api_call(scode, matcode_val, "" + flt3, rem_length_str, moid, rem_val);
                                                    }
                                                }
                                                else
                                                {
                                                    if (org_idval.Equals(temp_idval))
                                                    {
                                                        int rem_val = 1;
                                                        if (rem_length_str.Equals("0"))
                                                        {
                                                            rem_val = 0;
                                                        }
                                                        if (rem_val == 1)
                                                        {
                                                            temp_serial_val = temp_serial_val + "," + scode;
                                                            EventViewer.WriteLog("Serial Code->" + temp_serial_val);
                                                            
                                                            EventViewer.WriteLog(scode + "->" + temp_serial_val + "->" + flt3 + "->" + rem_length_str + "->" + moid + "->" + rem_val);
                                                            string query = string.Format(@"UPDATE setupsheetmhi3 SET serialno='{1}' where sid='{0}' AND serialno !='{2}'", item.Value.fileid + "-" + moid + "$" + partno, temp_serial_val, scode);
                                                            using (MySqlCommand command = new MySqlCommand(query, mcon2))
                                                            {
                                                                int row = command.ExecuteNonQuery();
                                                                if (row == 1)
                                                                {
                                                                    exnest_api_call(scode, matcode_val, "" + flt3, rem_length_str, moid, rem_val);

                                                                    EventViewer.WriteLog("Updated MO(1710):" + moid);

                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                                break;
                                            }
                                        }
                                    }

                                }
                            }

                            if (material == "")
                            {
                                html = string.Empty;

                                
                                                 url = @"https://" + DBContext.getDomainName + "/api/get_alfaerp_data/SCHEDULERORDERV2?key=" + st_key + "&pids="+ id_4000+ "&page=0&limit=100&ps=-1,0,4,1,2,3&sv="+moid+"&ds=" + fromdate + "&de=" + todate + "&df==deliveryDate,processDate";
                                                //EventViewer.WriteLog(url);
                                                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                                 request = (HttpWebRequest)WebRequest.Create(url);
                                                request.AutomaticDecompression = DecompressionMethods.GZip;

                                                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                                                using (Stream stream = response.GetResponseStream())
                                                using (StreamReader reader = new StreamReader(stream))
                                                {
                                                    html = reader.ReadToEnd();

                                                    dynamic val_response = JObject.Parse(html);
                                                    int tot_count_val = val_response.count;
                                                    //EventViewer.WriteLog("" + tot_count_val);
                                                    for (int i = 0; i < tot_count_val; i++)
                                                    {
                                                        //var cust_name = val_response.mos[i].customerName;
                                                        // if (cust_name == "MHI-VA") {
                                                        for (int j = 0; j < val_response.mos[i].itemList.Count; j++)
                                                        {
                                                            string prodno = val_response.mos[i].itemList[j].productNo;
                                                            string mono = val_response.mos[i].itemList[j].smi_mo_number;

                                                            if (prodno == partno && mono == moid)
                                                            {
                                                                partLength = val_response.mos[i].itemList[j].partLength;
                                                                partWidth = val_response.mos[i].itemList[j].partWidth;
                                                                partThickness = val_response.mos[i].itemList[j].partThickness;
                                                                smimodel = val_response.mos[i].itemList[j].smi_model_number;
                                                                material = val_response.mos[i].itemList[j].smi_alloy;
                                                                shape = val_response.mos[i].itemList[j].smi_shape;
                                                                EventViewer.WriteLog("4000-" + material);
                                                                matcode_val = val_response.mos[i].itemList[j].materialCode;
                                                                duedate = val_response.mos[i].itemList[j].dueDate;
                                                                intduedate = val_response.mos[i].itemList[j].internal_due_date;
                                                                matwidth = val_response.mos[i].itemList[j].materialWidth;
                                                                matlength = val_response.mos[i].itemList[j].materialLength;
                                                                jobduedate = duedate;
                                                                final_jobdate = duedate;
                                                                final_intdate = duedate;

                                                                if (!String.IsNullOrEmpty(intduedate))
                                                                {
                                                                    DateTime temp = DateTime.ParseExact(intduedate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                                                                    final_intdate = temp.ToString("yyyy-MM-dd");
                                                                }
                                                                if (val_response.mos[i].itemList[j].processList.Count > 0)
                                                                {
                                                                    orderid = val_response.mos[i].itemList[j].processList[0].id;
                                                                    jobduedate = val_response.mos[i].itemList[j].processList[0].endDate;
                                                                    if (!String.IsNullOrEmpty(jobduedate))
                                                                    {
                                                                        DateTime temp = DateTime.ParseExact(jobduedate, "MM/dd/yy HH:mm:ss tt", CultureInfo.InvariantCulture);
                                                                        final_jobdate = temp.ToString("yyyy-MM-dd");
                                                                    }
                                                                }

                                                                string CmdText1 = "INSERT IGNORE INTO setupsheetmhi4 VALUES(@sid,@machinename,@materialname,@materialsize,@filename,@date,@processed,@sheets,@processingtime,@starttime,@endtime,@programname,@pduedate,@intduedate,@qty,@partnum,@compid,@partname,@comment,@orderno,@sheetcode,@toolinfo,@serialno,@priority,@pdfdownloaded,@partthickness,@partwidth,@partlength,@smimodel,@orderid,@suspendval,@shape)";
                                                                MySqlCommand cmd = new MySqlCommand(CmdText1, mcon2);
                                                                cmd.Parameters.AddWithValue("@sid", item.Value.fileid + "-" + moid + "$" + partno);
                                                                cmd.Parameters.AddWithValue("@machinename", "FLC3015AJ");
                                                                cmd.Parameters.AddWithValue("@materialname", material);
                                                                cmd.Parameters.AddWithValue("@materialsize", matwidth + "x" + matlength);
                                                                cmd.Parameters.AddWithValue("@filename", "");
                                                                cmd.Parameters.AddWithValue("@sheets", 1);
                                                                cmd.Parameters.AddWithValue("@processingtime", "");
                                                                cmd.Parameters.AddWithValue("@starttime", "0000-00-00 00:00:00");
                                                                cmd.Parameters.AddWithValue("@endtime", "0000-00-00 00:00:00");
                                                                cmd.Parameters.AddWithValue("@comment", matcode_val);
                                                                cmd.Parameters.AddWithValue("@programname", moid);
                                                                cmd.Parameters.AddWithValue("@partnum", partno);
                                                                cmd.Parameters.AddWithValue("@partname", "");
                                                                cmd.Parameters.AddWithValue("@qty", tot_count);
                                                                cmd.Parameters.AddWithValue("@orderno", moid);
                                                                cmd.Parameters.AddWithValue("@compid", "841");
                                                                cmd.Parameters.AddWithValue("@processed", 0);
                                                                cmd.Parameters.AddWithValue("@pduedate", final_jobdate);
                                                                cmd.Parameters.AddWithValue("@intduedate", final_intdate);
                                                                cmd.Parameters.AddWithValue("@date", c_date);
                                                                cmd.Parameters.AddWithValue("@sheetcode", "");
                                                                cmd.Parameters.AddWithValue("@toolinfo", "");
                                                                cmd.Parameters.AddWithValue("@serialno", scode);
                                                                cmd.Parameters.AddWithValue("@priority", 0);
                                                                cmd.Parameters.AddWithValue("@pdfdownloaded", 1);
                                                                cmd.Parameters.AddWithValue("@partthickness", partThickness);
                                                                cmd.Parameters.AddWithValue("@partwidth", partWidth);
                                                                cmd.Parameters.AddWithValue("@partlength", partLength);
                                                                cmd.Parameters.AddWithValue("@smimodel", smimodel);
                                                                cmd.Parameters.AddWithValue("@orderid", orderid);
                                                                cmd.Parameters.AddWithValue("@suspendval", 0);
                                                                cmd.Parameters.AddWithValue("@shape", shape);
                                                                int result = cmd.ExecuteNonQuery();
                                                                string org_idval = moid + "$" + partno;
                                                                if (result == 1)
                                                                {
                                                                    temp_idval = moid + "$" + partno;
                                                                    
                                                                    temp_serial_val = scode;
                                                                    temp_moid_val = moid;
                                                                    
                                                                    int rem_val = 1;
                                                                    if (rem_length_str.Equals("0"))
                                                                    {
                                                                        rem_val = 0;
                                                                    }
                                                                    if(rem_val == 1) { 
                                                                    EventViewer.WriteLog(scode + "->" + matcode_val + "->" + flt3 + "->" + rem_length_str + "->" + moid + "->" + rem_val);
                                                                    exnest_api_call(scode, matcode_val, "" + flt3, rem_length_str, moid, rem_val);
                                                                    }
                                                }else
                                                {
                                                    if (org_idval.Equals(temp_idval))
                                                    {
                                                        int rem_val = 1;
                                                        if (rem_length_str.Equals("0"))
                                                        {
                                                            rem_val = 0;
                                                        }
                                                        if (rem_val == 1)
                                                        {
                                                            temp_serial_val = temp_serial_val + "," + scode;
                                                            EventViewer.WriteLog("Serial Code->" + temp_serial_val);
                                                            
                                                            EventViewer.WriteLog(scode + "->" + matcode_val + "->" + flt3 + "->" + rem_length_str + "->" + moid + "->" + rem_val);
                                                            string query = string.Format(@"UPDATE setupsheetmhi4 SET serialno='{1}' where sid='{0}' AND serialno !='{2}'", item.Value.fileid + "-" + moid + "$" + partno, temp_serial_val, scode);
                                                            using (MySqlCommand command = new MySqlCommand(query, mcon2))
                                                            {
                                                                int row = command.ExecuteNonQuery();
                                                                if (row == 1)
                                                                {
                                                                    exnest_api_call(scode, matcode_val, "" + flt3, rem_length_str, moid, rem_val);

                                                                    EventViewer.WriteLog("Updated MO(4000):" + moid);

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                                break;
                                                            }
                                                        }
                                                    }

                                                }

                                            }
                                        
                                

                            
                        }
                    }

                }
            }
            catch (Exception e)
            {
                EventViewer.WriteLog(e);
                if (e.ToString().Contains("Fatal"))
                {
                    f_error = 1;
                }
            }
            finally
            {
                if (f_error == 0)
                {
                    EventViewer.WriteFileLog("" + item.Value.fileid);
                }
                if (File.Exists(path))
                    File.Delete(path);
            }
        }
        
        public static void extract_tool_data_mhi(string path, MySqlConnection mcon2, KeyValuePair<String, Upload.data> item)
        {
            try
            {
                using (StreamReader csv1 = new StreamReader(File.OpenRead(path)))
                {
                    int counter = 0;
                    while (!csv1.EndOfStream)
                    {
                        counter++;
                        var line = csv1.ReadLine();
                        var csv = line.Split(',');
                        if (counter > 1)
                        {
                            string pgmno = csv[0];

                            string tool_final_data = "";
                            for (int i = 1; i < csv.Count(); i++)
                            {
                                if (!string.IsNullOrEmpty(csv[i])) {
                                    if (!string.IsNullOrEmpty(tool_final_data))
                                        tool_final_data = tool_final_data + "," + csv[i] ;
                                    else
                                        tool_final_data = tool_final_data+csv[i];
                                }else
                                {
                                    if (!string.IsNullOrEmpty(tool_final_data))
                                        tool_final_data = tool_final_data + "," + "N/A";
                                    else
                                        tool_final_data = tool_final_data + "N/A";

                                }
                            }
                           


                            string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetmhi1 SET toolinfo='{1}' where programname='{0}'", pgmno, tool_final_data);


                            using (MySqlCommand command = new MySqlCommand(query, mcon2))
                            {
                                try
                                {
                                    int row = command.ExecuteNonQuery();
                                    if (row == 1)
                                    {
                                        EventViewer.WriteFileLog("" + item.Value.fileid);
                                    }
                                }catch(Exception e)
                                {
                                    EventViewer.WriteLog(e);
                                }
                            }
                            
                        }
                    }

                }
            }
            catch (Exception e)
            {
                EventViewer.WriteLog(e);
            }
            finally
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
        }
        private async static Task uploadContents(KeyValuePair<String, Upload.data> item)
        {
            String sqlFormattedDate = DateTime.Now.ToString("yyyy-MM-dd");

            try
            {
                if (mcon_setup.State == ConnectionState.Closed)
                {
                    mcon_setup.Open();
                }
                extract_process_info();
                //update_gnn_mo_id(mcon_setup);
                string extension = System.IO.Path.GetExtension(item.Value.filename);
                string filename_str = System.IO.Path.GetFileNameWithoutExtension(item.Value.filename);
                var filestream = await _s3.DownloadFileAsStream(S3Storage.FilesBucket, item.Value.guid);
                if (filename_str.StartsWith("ex"))
                {
                    string file_str_name = item.Value.filename;
                    string path = SaveFile_CSV(filestream, item.Value.filename);
                    extract_gnn_çut_data_mhi(path, mcon_setup, item);
                }
                else if (extension.Contains("csv") || extension.Contains("CSV"))
                {
                    string file_str_name = item.Value.filename;
                    string path = SaveFile_CSV(filestream, item.Value.filename);

                    if (file_str_name.StartsWith("TOL"))
                    {
                        extract_tool_data_mhi(path, mcon_setup, item);
                    }
                    else
                    {
                        //EventViewer.WriteFileLog("" + item.Value.fileid);
                        extract_gnn_data_mhi(path, mcon_setup, item);
                    }
                }
                else if(extension.Contains("pdf") || extension.Contains("PDF"))
                {
                    //if (!string.IsNullOrEmpty(item.Value.thumbguid))
                    //{
                        EventViewer.WriteFileLog("" + item.Value.fileid);
                   // }
                    string file_path = SaveFile_PDF(filestream, item.Value.filename);
                    using (PdfReader reader = new PdfReader(file_path))
                    {
                        try
                        {
                            GhostscriptPngDevice dev = new GhostscriptPngDevice(GhostscriptPngDeviceType.Png16m);
                            dev.GraphicsAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
                            dev.TextAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
                            dev.ResolutionXY = new GhostscriptImageDeviceResolution(200, 200);
                            dev.InputFiles.Add(file_path);
                            dev.Pdf.FirstPage = 1;
                            dev.Pdf.LastPage = 1;
                            dev.CustomSwitches.Add("-dDOINTERPOLATE");

                            dev.OutputPath = System.IO.Path.Combine(@"F:\xampp\htdocs\alfadockpro\factory_layout\1\setupsheet\pdf\841\png\", filename_str+"_original.png");
                            dev.Process();
                            using (var originalImage = new Bitmap(dev.OutputPath))
                            {
                                Rectangle ProgramName = new Rectangle(400, 375, 900, 700);
                                Bitmap croppedImage1 = originalImage.Clone(ProgramName, originalImage.PixelFormat);
                                croppedImage1.Save(@"F:\xampp\htdocs\alfadockpro\factory_layout\1\setupsheet\pdf\841\png\" + filename_str + ".png", ImageFormat.Png);

                                croppedImage1.Dispose();

                            }

                        }
                        catch (Exception ex)
                        {
                            EventViewer.WriteLog("Exception in cropimage- Msg-" + ex.Message + Environment.NewLine + ex.StackTrace);

                        }
                    }
                       
                    var png_filestream = await _s3.DownloadFileAsStream(S3Storage.FilesBucket, item.Value.thumbguid);
                    SaveFile_PDF(png_filestream, System.IO.Path.GetFileNameWithoutExtension(item.Value.filename) +".png");
                }else
                {
                    EventViewer.WriteFileLog("" + item.Value.fileid);
                }

                filestream.Dispose();
                removeItemFromDb(item, 1);

            }


            catch (Exception ex)
            {
                removeItemFromDb(item, 2);
                EventViewer.WriteLog("Exception in uploadContents- Msg-" + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                
            }
        }
        static Boolean checkFileId(String fileid)
        {
            String sqlFormattedDate = DateTime.Now.ToString("yyyy-MM-dd");
            string file_id_log = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar + "fileid_log_" + sqlFormattedDate + ".txt";

            bool isMatch = false;
            using (StreamReader sr = File.OpenText(file_id_log))
            {
                string[] lines = File.ReadAllLines(file_id_log);
                for (int x = 0; x <= lines.Length - 1; x++)
                {
                    if (fileid == lines[x])
                    {
                        sr.Close();
                        isMatch = true;
                        break;
                    }
                }
                if (!isMatch)
                {
                    sr.Close();
                }
            }
            return isMatch;
        }
        private class FileData
        {
            public string filePath { get; set; }
            public string fileName { get; set; }
            public string ftype { get; set; }
            public string userid { get; set; }
            //SetupFile Information
            public string pName = "";
            public string partName = "";
            public string macName = "";
            public string matName = "";
            public string matSize = "";
            public string qty = "0";
            public string due = "";
            public int numOfSheets;
            public string processTime = "";
            public string partno = "";
            public string prodname = "";
            public string prodno = "";
            public string toolinfo = "";
            public string orderno = "";
            public string customer = "";
            public string date = "";
            public string pdue = "";
            public string compid = "";
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="setupsheetdata"></param>
        /// <param name="item"></param>
        public static string SaveFile_CSV(Stream stream, string filename)
        {
            string filePath = System.IO.Path.Combine(@"C:\setap100\", filename);
            try
            {
                using (var filestr = System.IO.File.Create(filePath))
                {
                    stream.CopyTo(filestr);
                    filestr.Flush();
                }
                stream.Dispose();
                return filePath;
            }
            catch (Exception e)
            {
                EventViewer.WriteLog("OFcPath : Failed" + e.StackTrace + " ");
            }
            return null;
        }
        public static string SaveFile_PDF(Stream stream, string filename)
        {
            string filePath = System.IO.Path.Combine(@"F:\xampp\htdocs\alfadockpro\factory_layout\1\setupsheet\pdf\841\", filename);
            try
            {
                using (var filestr = System.IO.File.Create(filePath))
                {
                    stream.CopyTo(filestr);
                    filestr.Flush();
                }
                stream.Dispose();
                return filePath;
            }
            catch (Exception e)
            {
                EventViewer.WriteLog("OFcPdfPath : Failed" + e.StackTrace + " ");
            }
            return null;
        }


        public void addItem(Upload.data data)
        {
            lock (padlock)
            {
                if (dictFiles.ContainsKey("" + data.fileid))
                {

                }
                else
                {
                    dictFiles.TryAdd("" + data.fileid, data);
                }
            }

        }

        public void removeItem(KeyValuePair<string, Upload.data> item)
        {
            Upload.data data = null;

            lock (padlock)
            {
                if (dictFiles.ContainsKey(item.Key))
                {
                    dictFiles.TryRemove(item.Key, out data);
                }

            }


        }

        private static void removeItemFromDb(KeyValuePair<string, Upload.data> item, int completed)
        {
            //completed : 1-success, 2-failed
            // remove the item from db
            try
            {
                if (completed != 2)
                {
                    // _dbContentSearch.updateConvertedItem(item, completed);
                    //_dbContentSearch.updatesheetinfo(item);
                }

                Instance.removeItem(item);
                if (Instance.getItemsCount() <= 0)
                {
                    if (!Global.timer.Enabled)
                        Global.timer.Enabled = true;
                }
                //_dbContentSearch.getOpenItems();
            }
            catch (Exception ex)
            {
                //_dbContentSearch.getOpenItems();
                EventViewer.WriteLog("Exception in removeItemFromDb- Msg-" + ex.Message + Environment.NewLine + ex.StackTrace);
            }

        }

        public int getItemsCount()
        {
            lock (padlock)
            {
                return dictFiles.Count();
            }
        }

    }
}
