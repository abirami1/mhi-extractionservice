﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mhiextractionService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        public static bool isDebugMode = false;

        static void Main()
        {
            // debug mode to test; release mode to build service
#if (DEBUG)
                isDebugMode = true;
#endif

            EventViewer.WriteLog("inside main");
            if (isDebugMode)
            {
                EventViewer.WriteLog("Debug Mode:");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new DebugForm());
            }
            else
            {
                EventViewer.WriteLog("inside service start");
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new ExtractionService()
                };
                ServiceBase.Run(ServicesToRun);
            }
            
        }
    }
}
